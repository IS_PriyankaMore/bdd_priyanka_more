package com.assignment1.membersofrestaurant;

import java.util.Scanner;

public class Owner extends Manager {

	private String name;
	private String address;
	private String dob; // please enter in format DD/MM/YYYY
	private String gender;

	public void addOwner() {
		System.out.println("Add Owner Here>>>");
		Scanner sc = new Scanner(System.in); // Create a Scanner object
		System.out.println("Enter name");
		name = sc.nextLine();
		System.out.println("Enter address");
		address = sc.nextLine();
		System.out.println("Enter gender");
		gender = sc.nextLine();
		System.out.println("Enter DOB");
		dob = sc.nextLine();
	}

	public void displayOwnerDetails() {
		System.out.println("Owner Added Successfully>>>>>>");
		System.out.println("This are the details you entered : ");
		System.out.println("Employee name: " + name);
		System.out.println("Address: " + address);
		System.out.println("D.O.B: " + dob);
		System.out.println("Gender: " + gender);
		
	}

// Owner can add ,remove manager and employees here
	@Override
	public void createNewManager() {
		super.createNewManager();
	}

	@Override
	public void displayManager() {
		super.displayManager();
	}

	@Override
	public void deleteManager() {
		super.deleteManager();
	}

	@Override
	public void addEmployee() {
		super.addEmployee();
	}

	@Override
	public void displayEmployeeDetails() {
		super.displayEmployeeDetails();
	}

	@Override
	public void removeEmployee() {
		super.removeEmployee();
	}

//Owner can add new assistant manager
	public void addNewAssistantManager() {
		AssistantManager am = new AssistantManager();
		am.addNewAssistantManager();

	}

//Owner can remove assistant manager 
	public void removeAssistantManager() {
		AssistantManager am = new AssistantManager();
		am.deleteAssistantManager();
	}
}