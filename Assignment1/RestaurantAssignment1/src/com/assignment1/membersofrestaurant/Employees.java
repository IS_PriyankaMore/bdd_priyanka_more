package com.assignment1.membersofrestaurant;

import java.util.Scanner;

public class Employees {

	private String name;
	private String address;
	private String dob; // please enter in format DD/MM/YYYY
	private String gender;
	private String department;
	private String workingShift;
	private String salary;
	private String WorkProfile;

	public String getWorkProfile() {
		return WorkProfile;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getDob() {
		return dob;
	}

	public String getGender() {
		return gender;
	}

	public String getDepartment() {
		return department;
	}

	public String getWorkingShift() {
		return workingShift;
	}

	public String getSalary() {
		return salary;
	}

	// method to add details of employees

	public void addEmployee() {
		System.out.println("Add Employee Here>>>");
		Scanner sc = new Scanner(System.in); // Create a Scanner object
		System.out.println("Enter name");
		name = sc.nextLine();
		System.out.println("Enter address");
		address = sc.nextLine();
		System.out.println("Enter salary");
		salary = sc.nextLine();
		System.out.println("Enter DOB");
		dob = sc.nextLine();
		System.out.println("Enter working Shift ");
		workingShift = sc.nextLine();
		System.out.println("Enter Department");
		department = sc.nextLine();
		System.out.println("Enter Gender");
		gender = sc.nextLine();
		System.out.println("Enter work profile");
		WorkProfile = sc.nextLine();

	}

	public void displayEmployeeDetails() {
		System.out.println("Employee Added Successfully>>>>>>");
		System.out.println("This are the details you entered : ");
		System.out.println("Employee name: " + getName());
		System.out.println("Address: " + getAddress());
		System.out.println("D.O.B: " + getDob());
		System.out.println("Gender: " + getGender());
		System.out.println("department: " + getDepartment());
		System.out.println("workingShift" + getWorkingShift());
		System.out.println("salary" + getSalary());
		System.out.println("Work Profile " + getWorkProfile());

	}

	public void removeEmployee() {
		System.out.println("Enter details of employee to be removed >>>>");
		Scanner sc = new Scanner(System.in); // Create a Scanner object
		System.out.println("Enter name");
		name = sc.nextLine();
		System.out.println("Enter DOB");
		dob = sc.nextLine();
		System.out.println("Employee  " + name + " Removed Successfully ");
	}

}
