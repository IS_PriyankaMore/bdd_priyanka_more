package com.assignment1.membersofrestaurant;

public class AssistantManager extends Manager {
//Overridden Methods of Manager Class
	@Override
	public void createNewManager() {
		super.createNewManager();
	}

	@Override
	public void displayManager() {
		super.displayManager();
	}

//Overridden Methods of Employees Class
	@Override
	public void addEmployee() {
		super.addEmployee();
	}

	@Override
	public void displayEmployeeDetails() {
		super.displayEmployeeDetails();
	}

	@Override
	public void removeEmployee() {
		super.removeEmployee();
	}

// Methods for assistant Manager class
	public void addNewAssistantManager() {
		AssistantManager am = new AssistantManager();
		am.createNewManager();
		am.displayManager();
	}

	public void deleteAssistantManager() {
		AssistantManager am = new AssistantManager();
		am.deleteManager();
	}
}