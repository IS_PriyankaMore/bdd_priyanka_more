package com.assignment1.restaurantdetails;

import com.assignment1.departments.Accounting;
import com.assignment1.departments.Bar;
import com.assignment1.departments.Department;
import com.assignment1.departments.Dining;
import com.assignment1.departments.Kitchen;
import com.assignment1.departments.Reception;
import com.assignment1.departments.Security;
import com.assignment1.membersofrestaurant.AssistantManager;
import com.assignment1.membersofrestaurant.Manager;
import com.assignment1.membersofrestaurant.Owner;

public class RestaurantWorking {

	public static void main(String[] args) {

		// Departments in Restaurant

		Department deptartment = new Department();
		deptartment.showDepartmentsAvailable();
		deptartment.totalNumberOfEmployeesWorkingInsideThisDepartment();
		deptartment.totalNumberOfDepartments();

		// Accounting Department
		Accounting accounting = new Accounting();
		accounting.addNewEmployeeinThisDepartment();
		accounting.totalNumberOfEmployeesWorkingInsideThisDepartment();
		accounting.totalNumberOfWorkProfilesInThisDepartment();
		accounting.showWorkProfiles();
		accounting.removeNewEmployeeFromThisDepartment();

		// Bar Department
		Bar bar = new Bar();
		bar.addNewEmployeeinThisDepartment();
		bar.totalNumberOfEmployeesWorkingInsideThisDepartment();
		bar.totalNumberOfWorkProfilesInThisDepartment();
		bar.showWorkProfiles();
		bar.removeNewEmployeeFromThisDepartment();

		// Dining Department

		Dining dining = new Dining();
		dining.addNewEmployeeinThisDepartment();
		dining.totalNumberOfEmployeesWorkingInsideThisDepartment();
		dining.totalNumberOfWorkProfilesInThisDepartment();
		dining.showWorkProfiles();
		dining.removeNewEmployeeFromThisDepartment();

		// Kitchen Department

		Kitchen kitchen = new Kitchen();
		kitchen.addNewEmployeeinThisDepartment();
		kitchen.totalNumberOfEmployeesWorkingInsideThisDepartment();
		kitchen.totalNumberOfWorkProfilesInThisDepartment();
		kitchen.showWorkProfiles();
		kitchen.removeNewEmployeeFromThisDepartment();

		// Reception Department
		Reception reception = new Reception();
		reception.addNewEmployeeinThisDepartment();
		reception.totalNumberOfEmployeesWorkingInsideThisDepartment();
		reception.totalNumberOfWorkProfilesInThisDepartment();
		reception.showWorkProfiles();
		reception.removeNewEmployeeFromThisDepartment();

		// Security Department

		Security security = new Security();
		security.addNewEmployeeinThisDepartment();
		security.totalNumberOfEmployeesWorkingInsideThisDepartment();
		security.totalNumberOfWorkProfilesInThisDepartment();
		security.showWorkProfiles();
		security.removeNewEmployeeFromThisDepartment();

		// Owner Can perform Below Operations

		Owner owner = new Owner(); // Created Object of Owner Class
		owner.addOwner();
		owner.displayOwnerDetails();
		owner.createNewManager();
		owner.displayManager();
		owner.addNewAssistantManager();
		owner.addEmployee();
		owner.displayEmployeeDetails();
		owner.deleteManager();
		owner.removeAssistantManager();
		owner.removeEmployee();

		// Manager Can Perform Following Operations
		Manager manager = new Manager();
		manager.createNewManager();
		manager.displayManager();
		manager.addEmployee();
		manager.displayEmployeeDetails();
		manager.addNewAssistantManager();
		manager.deleteAssistantManager();
		manager.deleteManager();
		manager.removeEmployee();

		// Assistant Manager can Perform Following Operations
		AssistantManager assistantManager = new AssistantManager();
		assistantManager.addNewAssistantManager();
		assistantManager.addEmployee();
		assistantManager.removeEmployee();
		assistantManager.deleteAssistantManager();

	}
}
