package com.assignment1.departments;

import com.assignment1.membersofrestaurant.Employees;

public class Bar extends Department {
	int totalWorkProfiles;
	String workProfile1;
	String workProfile2;
	String workProfile3;
	String workProfile4;

	public void totalNumberOfEmployeesWorkingInsideThisDepartment() { //Bar Department Employees
		Department dept = new Department();
		employeesworking = dept.setEmployeesworking("12");
		System.out.println("Total No. of Employees inside Bar Department  =  " + employeesworking);
	}

	public void addNewEmployeeinThisDepartment() {
		Accounting ac = new Accounting();
		ac.addNewEmployeeinThisDepartment();
	}

	public void removeNewEmployeeFromThisDepartment() {
		Employees emp = new Employees();
		emp.removeEmployee();
	}

	public void showWorkProfiles() {
		Accounting accounting = new Accounting();
		workProfile1 = accounting.setWorkProfile1("Cashier");
		workProfile2 = accounting.setWorkProfile2("Waiter");
		workProfile3 = accounting.setWorkProfile3("Servicing Man");
		workProfile4 = accounting.setWorkProfile4("Assistant Manager");
		System.out.println("workProfile1 is  :>>   " + workProfile1);
		System.out.println("workProfile2 is  :>>   " + workProfile2);
		System.out.println("workProfile3 is  :>>   " + workProfile3);
		System.out.println("workProfile4 is  :>>   " + workProfile4);

	}

	public void totalNumberOfWorkProfilesInThisDepartment() {
		Bar bar = new Bar();
		totalWorkProfiles = bar.setTotalWorkProfiles(4);
		System.out.println("Total No. of Working People inside this Department >>> " + totalWorkProfiles);
	}

	public int setTotalWorkProfiles(int totalWorkProfiles) {
		return this.totalWorkProfiles = totalWorkProfiles;
	}
}
