package com.assignment1.departments;

import com.assignment1.membersofrestaurant.Employees;

public class Accounting extends Department {
	int totalWorkProfiles;
	String workProfile1;
	String workProfile2;
	String workProfile3;
	String workProfile4;

	public void totalNumberOfEmployeesWorkingInsideThisDepartment() { // Accounting Department Employees
		Department dept = new Department();
		employeesworking = dept.setEmployeesworking("4");
		System.out.println("Total No. of Employees inside Accounting Department  =  " + employeesworking);
	}

	public void totalNumberOfWorkProfilesInThisDepartment() { // Total Working Profiles
		Accounting accounting = new Accounting();
		totalWorkProfiles = accounting.setTotalWorkProfiles(4);
		System.out.println("Total No. of Departments inside this restaurant >>> " + totalWorkProfiles);
	}

	public void addNewEmployeeinThisDepartment() { // Add new employees in Accounting department
		System.out.println("Add new employee here>>>>  ");
		Employees emp = new Employees();
		emp.addEmployee(); // called method of employee class
		System.out.println("Show newly added Employee Here>>>>  ");
		emp.displayEmployeeDetails();
	}

	public void removeNewEmployeeFromThisDepartment() {
		Employees emp = new Employees();
		emp.removeEmployee(); // called method of employee class
	}

	public void showWorkProfiles() { // method to display work profiles of tid department
		Accounting accounting = new Accounting();
		workProfile1 = accounting.setWorkProfile1("Accountant");
		workProfile2 = accounting.setWorkProfile2("Cashier");
		workProfile3 = accounting.setWorkProfile3("Bill Keeper");
		workProfile4 = accounting.setWorkProfile4("Assistant Manager");
		System.out.println("workProfile1 is  :>>  " + workProfile1);
		System.out.println("workProfile2 is  :>>  " + workProfile2);
		System.out.println("workProfile3 is  :>>  " + workProfile3);
		System.out.println("workProfile4 is  :>>  " + workProfile4);

	}

	public int setTotalWorkProfiles(int totalWorkProfiles) {
		return this.totalWorkProfiles = totalWorkProfiles;
	}

	public String setWorkProfile1(String workProfile1) {
		return this.workProfile1 = workProfile1;
	}

	public String setWorkProfile2(String workProfile2) {
		return this.workProfile2 = workProfile2;
	}

	public String setWorkProfile3(String workProfile3) {
		return this.workProfile3 = workProfile3;
	}

	public String setWorkProfile4(String workProfile4) {
		return this.workProfile4 = workProfile4;
	}

}