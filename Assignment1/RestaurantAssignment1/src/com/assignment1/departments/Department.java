package com.assignment1.departments;

import java.util.Scanner;

public class Department {

	String Department1;
	String Department2;
	String Department3;
	String Department4;
	String Department5;
	String Department6;
	String employeesworking;

	public void totalNumberOfEmployeesWorkingInsideThisDepartment() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Total No. Of Emplyees working >>>");
		employeesworking = sc.nextLine();
		System.out.println("Total No. of Employees inside this department  = " + employeesworking);
	}

	public void totalNumberOfDepartments() {
		Department dept = new Department();
		totalDepartments = dept.setTotalDepartments(6);
		System.out.println("Total No. of Departments inside this restaurant >>> " + totalDepartments);
	}

	public void showDepartmentsAvailable() {
		Department d = new Department();
		Department1 = d.setDepartment1("Accounting");
		Department2 = d.setDepartment2("Bar");
		Department3 = d.setDepartment3("Reception");
		Department4 = d.setDepartment4("Dining");
		Department5 = d.setDepartment5("Security");
		Department6 = d.setDepartment6("Kitchen");

		System.out.println("Department 1 is  :>>  " + Department1);
		System.out.println("Department 2 is  :>>  " + Department2);
		System.out.println("Department 3 is  :>>  " + Department3);
		System.out.println("Department 4 is  :>>  " + Department4);
		System.out.println("Department 5 is  :>>  " + Department5);
		System.out.println("Department 6 is  :>>  " + Department6);

	}

	public String getEmployeesworking() {
		return employeesworking;
	}

	public String setEmployeesworking(String employeesworking) {
		return this.employeesworking = employeesworking;
	}

	public String getDepartment6() {
		return Department6;
	}

	public String setDepartment6(String department6) {
		return Department6 = department6;
	}

	public String getDepartment1() {
		return Department1;
	}

	public String setDepartment1(String department1) {
		return Department1 = department1;
	}

	public String getDepartment2() {
		return Department2;
	}

	public String setDepartment2(String department2) {
		return Department2 = department2;
	}

	public String getDepartment3() {
		return Department3;
	}

	public String setDepartment3(String department3) {
		return Department3 = department3;
	}

	public String getDepartment4() {
		return Department4;
	}

	public String setDepartment4(String department4) {
		return Department4 = department4;
	}

	public String getDepartment5() {
		return Department5;
	}

	public String setDepartment5(String department5) {
		return Department5 = department5;
	}

	int totalDepartments;

	public int getTotalDepartments() {
		return totalDepartments;
	}

	public int setTotalDepartments(int totalDepartments) {
		return this.totalDepartments = totalDepartments;
	}

}
