package com.assignment1.departments;

import com.assignment1.membersofrestaurant.Employees;

public class Security extends Department {

	int totalWorkProfiles;
	String workProfile1;
	String workProfile2;
	String workProfile3;

	public void addNewEmployeeinThisDepartment() {
		Accounting ac = new Accounting();
		ac.addNewEmployeeinThisDepartment();
	}

	public void removeNewEmployeeFromThisDepartment() {
		Employees emp = new Employees();
		emp.removeEmployee();
	}

	public void showWorkProfiles() {
		Accounting accounting = new Accounting();
		workProfile1 = accounting.setWorkProfile1("Security Guard");
		workProfile2 = accounting.setWorkProfile2("Watchmen");
		workProfile3 = accounting.setWorkProfile3("Assistant Manager");

		System.out.println("workProfile1 is  :>>  " + workProfile1);
		System.out.println("workProfile2 is  :>>  " + workProfile2);
		System.out.println("workProfile3 is  :>>  " + workProfile3);

	}

	public void totalNumberOfWorkProfilesInThisDepartment() {
		Security security = new Security();
		totalWorkProfiles = security.setTotalWorkProfiles(3);
		System.out.println("Total No. of Working People inside Security Department >>> " + totalWorkProfiles);
	}

	public int setTotalWorkProfiles(int totalWorkProfiles) {
		return this.totalWorkProfiles = totalWorkProfiles;
	}

	public void totalNumberOfEmployeesWorkingInsideThisDepartment() {
		Department dept = new Department();
		employeesworking = dept.setEmployeesworking("6");
		System.out.println("Total No. of Employees inside Security Department  =  " + employeesworking);
	}
}
