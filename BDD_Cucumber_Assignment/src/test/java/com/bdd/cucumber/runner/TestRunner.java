package com.bdd.cucumber.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

//Test Runner class used to run the step definition with appropriate scenario in feature file
//Provided feature file path as per execution and created different report for different feature.
//Uncomment the Lines as per requirement of execution.

/*
@RunWith(Cucumber.class)
@CucumberOptions(features = "C:\\Users\\priyanka.local\\BDD_Cucumber Assignment\\src\\test\\java\\com\\bdd\\cucumber\\feature\\files\\LoginToSite.Feature", glue = {
		"com.bdd.cucumber.step.defination" }, format ={"json:target/Destination/CucumberReportWithTag.json", "pretty", "html:test-output" }, dryRun = false,tags = {"@SmokeTest"},monochrome = true)
*/

/*@RunWith(Cucumber.class)
@CucumberOptions(features = "C:\\Users\\priyanka.local\\BDD_Cucumber Assignment\\src\\test\\java\\com\\bdd\\cucumber\\feature\\files\\LoginToSite.Feature", glue = {
		"com.bdd.cucumber.step.defination" }, format ={"json:target/Destination/CucumberReportWithExcludingTag.json", "pretty", "html:test-output" }, dryRun = false,tags = {"~@SmokeTest"},monochrome = true)
*/
/*@RunWith(Cucumber.class)
@CucumberOptions(features = "C:\\Users\\priyanka.local\\BDD_Cucumber Assignment\\src\\test\\java\\com\\bdd\\cucumber\\feature\\files\\LoginWithScenarioOutline.Feature", glue = {
		"com.bdd.cucumber.step.defination" }, format ={"json:target/Destination/CucumberWithScenarioOutline.json", "pretty", "html:test-output" }, dryRun = false,monochrome = true)

*/
@RunWith(Cucumber.class)
@CucumberOptions(features = "C:\\Users\\priyanka.local\\BDD_Cucumber Assignment\\src\\test\\java\\com\\bdd\\cucumber\\feature\\files\\RegisterToSite.Feature", glue = {
		"com.bdd.cucumber.step.defination" }, format = { "json:target/Destination/CucumberWithDataTables.json",
				"pretty", "html:test-output" }, dryRun = false, monochrome = true)

public class TestRunner {

}
