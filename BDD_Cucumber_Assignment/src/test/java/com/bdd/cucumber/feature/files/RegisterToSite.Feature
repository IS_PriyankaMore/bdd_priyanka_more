Feature: Register Using Data Table
Scenario: Register to Site

Given user is on a Register page
When user click on sign in link for register
When user enter email and click on create an account link
When user enter valid data on the page
|FirstName|LastName|Password|AddFname|AddLname|Company|CAddress		|City	 |Zipcode|HomePh	|Mobile			|addressalias|
|Priyanka |More    |Priya@1 |Main Rd |Home1   |InfoPK |Santa Clora|London|94105  |34564345|9876543234 |My Home		 |
When user click on register link
Then The user registration should be successful
