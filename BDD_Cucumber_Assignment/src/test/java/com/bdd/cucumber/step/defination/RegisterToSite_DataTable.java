package com.bdd.cucumber.step.defination;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RegisterToSite_DataTable {

	WebDriver driver;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\priyanka.local\\BDD_Cucumber Assignment\\src\\test\\resources\\drivers\\chromedriver1.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
	}

	@Given("^user is on a Register page$")
	public void user_is_on_a_Register_page() {

		driver.manage().window().maximize();
		driver.get("http://automationpractice.com/index.php");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

	@When("^user click on sign in link for register$")
	public void user_click_on_sign_in_link_for_register() {
		driver.findElement(By.xpath("//a[@title='Log in to your customer account']")).click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

	@When("^user enter email and click on create an account link$")
	public void user_enter_email_and_click_on_create_an_account_link() {
		WebElement email = driver.findElement(By.xpath("//input[@name='email_create']"));
		email.sendKeys("priyanka444@gmail.com");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		WebElement createaccount = driver.findElement(By.xpath("//button//span//i[@class='icon-user left']"));
		Actions action = new Actions(driver);
		action.moveToElement(createaccount);
		createaccount.click();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		if (createaccount.isDisplayed()) {
			email.clear();
			email.sendKeys("morepiya23453@gmail.com");
			action.moveToElement(createaccount);
			createaccount.click();
		}
	}

	@When("^user enter valid data on the page$")
	public void user_enter_valid_data_on_the_page(DataTable table) throws InterruptedException {

		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		List<List<String>> data = table.asLists(String.class);
		driver.findElement(By.xpath("//div[@class='radio' and @id='uniform-id_gender2']")).click();
		driver.findElement(By.xpath("//input[@id='customer_firstname']")).sendKeys(data.get(1).get(0));
		driver.findElement(By.xpath("//input[@id='customer_lastname']")).sendKeys(data.get(1).get(1));
		driver.findElement(By.xpath("//input[@id='passwd']")).sendKeys(data.get(1).get(2));
		driver.findElement(By.xpath("//input[@id='company']")).sendKeys(data.get(1).get(5));
		driver.findElement(By.xpath("//input[@id='address1']")).sendKeys(data.get(1).get(6));
		driver.findElement(By.xpath("//input[@id='city']")).sendKeys(data.get(1).get(7));
		driver.findElement(By.xpath("//input[@id='postcode']")).sendKeys(data.get(1).get(8));
		driver.findElement(By.xpath("//input[@id='phone']")).sendKeys(data.get(1).get(9));
		driver.findElement(By.xpath("//input[@id='phone_mobile']")).sendKeys(data.get(1).get(10));
		driver.findElement(By.xpath("//input[@id='alias']")).sendKeys(data.get(1).get(11));
		Select days = new Select(driver.findElement(By.xpath("//select[@id='days']")));
		days.selectByValue("14");
		Select months = new Select(driver.findElement(By.xpath("//select[@id='months']")));
		months.selectByValue("11");
		Select years = new Select(driver.findElement(By.xpath("//select[@id='years']")));
		years.selectByValue("1994");
		Select state = new Select(driver.findElement(By.xpath("//select[@id='id_state']")));
		state.selectByVisibleText("California");
		Select country = new Select(driver.findElement(By.xpath("//select[@id='id_country']")));
		country.selectByVisibleText("United States");
	}

	@When("^user click on register link$")
	public void user_click_on_register_link() {
		driver.findElement(By.xpath("//button[@id='submitAccount']")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@Then("^The user registration should be successful$")
	public void The_user_registration_should_be_successful() {
		if (driver.getCurrentUrl().equalsIgnoreCase("http://automationpractice.com/index.php?controller=my-account")) {
			System.out.println("Registration is Sucessfull>>>>");
		} else {
			System.out.println("Registration Failed.....Please Try Again>>>");
		}
	}

	@After
	public void cleanUp() {
		driver.close();
	}

}