package com.bdd.cucumber.step.defination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginToSite {

	WebDriver driver;

	@Before
	public void setUp() {

		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\priyanka.local\\BDD_Cucumber Assignment\\src\\test\\resources\\drivers\\chromedriver1.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
	}

	@Given("^user is on login page$")
	public void user_is_on_login_page() {
		driver.manage().window().maximize();
		driver.get("http://automationpractice.com/index.php");
	}

	@When("^user click on sign in link$")
	public void user_click_on_sign_in_link() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//a[@title='Log in to your customer account']")).click();
		String ActaulTitle = driver.getTitle();
		System.out.println("Actual Title is  :" + ActaulTitle);
		String ExpTitle = "Login - My Store";
		System.out.println("Expected Title is  :" + ExpTitle);
		if (!(ActaulTitle.contains(ExpTitle))) {
			System.out.println("Login Page Not Opened>>>");

		} else {
			System.out.println("Login Page Opened>>>");
		}
	}

	@When("^user enters username and password$")
	public void user_enters_username_and_password() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys("priyankamore141194@gmail.com");
		driver.findElement(By.xpath("//input[@id='passwd']")).sendKeys("Priya@1");
	}

	@When("^user clicks  on login button$")
	public void user_clicks_on_login_button() {
		driver.findElement(By.xpath("//button[@id='SubmitLogin']")).click();
	}

	@Then("^user is on home page$")
	public void user_is_on_home_page() {
		String ActualTitle = driver.getTitle();
		System.out.println("Actual Title is  : " + ActualTitle);
		String ExpTitle = "My account - My Store";
		System.out.println("Expected Title is :" + ExpTitle);
		if (!(ActualTitle.contains(ExpTitle))) {
			System.out.println("Login Failed...Please Try again >>>");
		} else {
			System.out.println("Login Succesfull>>>");

		}
	}

	@After
	public void cleanUp() {
		driver.quit();
	}
}
