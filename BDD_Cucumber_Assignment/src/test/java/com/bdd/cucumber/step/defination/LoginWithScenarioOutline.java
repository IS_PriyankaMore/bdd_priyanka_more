package com.bdd.cucumber.step.defination;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class LoginWithScenarioOutline {

	WebDriver driver;

	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver",
				"C:\\Users\\priyanka.local\\BDD_Cucumber Assignment\\src\\test\\resources\\drivers\\chromedriver1.exe");
		driver = new ChromeDriver();
		driver.manage().deleteAllCookies();
	}

	@Given("^User is on login page of Site$")
	public void user_is_on_login_page_of_site() {

		driver.manage().window().maximize();
		driver.get("http://automationpractice.com/index.php");
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	}

	@When("^Title of web page is verified$")
	public void title_of_web_page_is_verified() {
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//a[@title='Log in to your customer account']")).click();
		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("Login - My Store", title);
	}

	@When("^User enter Username as \"([^\"]*)\" and Password as \"([^\"]*)\"$")
	public void User_enter_Username_as_and_Password_as(String arg1, String arg2) {
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//input[@id='email']")).sendKeys(arg1);
		driver.findElement(By.xpath("//input[@id='passwd']")).sendKeys(arg2);
		driver.findElement(By.xpath("//button[@id='SubmitLogin']")).click();
	}

	@Then("^login should be unsuccessful$")
	public void login_should_be_unsuccessful() {
		if (driver.getCurrentUrl().equalsIgnoreCase("http://automationpractice.com/index.php?controller=my-account")) {
			System.out.println("Test Case Failed");
		} else {
			System.out.println("Test Case Passed");
		}
	}

	@After
	public void cleanUp() {
		driver.close();
	}

}
