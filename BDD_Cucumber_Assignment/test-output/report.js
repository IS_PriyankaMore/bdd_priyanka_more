$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/priyanka.local/BDD_Cucumber Assignment/src/test/java/com/bdd/cucumber/feature/files/RegisterToSite.Feature");
formatter.feature({
  "line": 1,
  "name": "Register Using Data Table",
  "description": "",
  "id": "register-using-data-table",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4373882900,
  "status": "passed"
});
formatter.before({
  "duration": 3288741000,
  "status": "passed"
});
formatter.before({
  "duration": 3235080700,
  "status": "passed"
});
formatter.scenario({
  "line": 2,
  "name": "Register to Site",
  "description": "",
  "id": "register-using-data-table;register-to-site",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "user is on a Register page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "user click on sign in link for register",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "user enter email and click on create an account link",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user enter valid data on the page",
  "rows": [
    {
      "cells": [
        "FirstName",
        "LastName",
        "Password",
        "AddFname",
        "AddLname",
        "Company",
        "CAddress",
        "City",
        "Zipcode",
        "HomePh",
        "Mobile",
        "addressalias"
      ],
      "line": 8
    },
    {
      "cells": [
        "Priyanka",
        "More",
        "Priya@1",
        "Main Rd",
        "Home1",
        "InfoPK",
        "Santa Clora",
        "London",
        "94105",
        "34564345",
        "9876543234",
        "My Home"
      ],
      "line": 9
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "user click on register link",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "The user registration should be successful",
  "keyword": "Then "
});
formatter.match({
  "location": "RegisterToSite_DataTable.user_is_on_a_Register_page()"
});
formatter.result({
  "duration": 13471893300,
  "status": "passed"
});
formatter.match({
  "location": "RegisterToSite_DataTable.user_click_on_sign_in_link_for_register()"
});
formatter.result({
  "duration": 6004651100,
  "status": "passed"
});
formatter.match({
  "location": "RegisterToSite_DataTable.user_enter_email_and_click_on_create_an_account_link()"
});
formatter.result({
  "duration": 505913000,
  "status": "passed"
});
formatter.match({
  "location": "RegisterToSite_DataTable.user_enter_valid_data_on_the_page(DataTable)"
});
formatter.result({
  "duration": 6177937400,
  "status": "passed"
});
formatter.match({
  "location": "RegisterToSite_DataTable.user_click_on_register_link()"
});
formatter.result({
  "duration": 3488762600,
  "status": "passed"
});
formatter.match({
  "location": "RegisterToSite_DataTable.The_user_registration_should_be_successful()"
});
formatter.result({
  "duration": 14575000,
  "status": "passed"
});
formatter.after({
  "duration": 76073700,
  "status": "passed"
});
formatter.after({
  "duration": 664807800,
  "status": "passed"
});
formatter.after({
  "duration": 72074500,
  "status": "passed"
});
});