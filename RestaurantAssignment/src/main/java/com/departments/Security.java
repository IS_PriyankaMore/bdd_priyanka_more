package main.java.com.departments;

import java.util.Iterator;
import java.util.Stack;

public class Security  {

	// Java Concepts Used : Stack ,Iterator

	public static void main(String args[]) {

		addNewEmployeeinThisDepartment();
	}

	// Method to add types of employees in this department
	public static void addNewEmployeeinThisDepartment() {

		Stack<String> stack = new Stack<String>();
		stack.push("Assistant Manager");
		stack.push("Security Guards");
		stack.push("Watchmen");
		stack.push("Parking Coordinator");
		stack.push("Helper");
		stack.pop();
		Iterator<String> itr = stack.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}

	}
}
