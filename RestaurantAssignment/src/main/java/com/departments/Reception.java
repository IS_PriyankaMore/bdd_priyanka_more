package main.java.com.departments;

import java.util.Iterator;
import java.util.Vector;


//Java Concepts Used : Vector ,Iterator
public class Reception  {

	public static void main(String args[]) {

		addNewEmployeeinThisDepartment();
	}

//Method to add types of employees in this department
	public static void addNewEmployeeinThisDepartment() {

		System.out.println("List of Employees Working in this Department :");

		Vector<String> v = new Vector<String>();
		v.add("Receptionist");
		v.add("Assistant Manager");
		v.add("Helper");
		Iterator<String> itr = v.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}
}