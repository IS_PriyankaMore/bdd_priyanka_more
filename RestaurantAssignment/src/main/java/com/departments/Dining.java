package main.java.com.departments;

import java.util.ArrayList;
import java.util.Iterator;

//Java Concepts Used : ArrayList ,Iterator
public class Dining  {

	// Method to add types of employees working in this department
	public static void addNewEmployeeinThisDepartment() {

		System.out.println("List of Employees Working in this Department :");
		ArrayList<String> list = new ArrayList<String>();
		list.add("Cashier");
		list.add("Assistant Manager");
		list.add("Waiters");
		list.add("Table Cleaner");

		Iterator itr = list.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

	public static void main(String[] args) {
		addNewEmployeeinThisDepartment();
	}

}
