package main.java.com.departments;

import java.util.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.io.*;

public class Accounting {
// Java Concepts Used : List ,Collections, Buffer Writer , Iterator

	// Method to add new working profile in accounting department
	public static void writeFile(String fileName, String empProfile) {
		try {

			BufferedWriter out = new BufferedWriter(new FileWriter(fileName, true));
			out.write(empProfile);
			System.out.println("Done");
			out.close();
		} catch (IOException e) {
			System.out.println("exception occoured" + e);
		}
	}

//Method to display working profiles in accounting department
	public static List<String> ReadFile(String fileName) {

		List<String> Profiles = Collections.emptyList();
		try {
			Profiles = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
		}

		catch (IOException e) {

			e.printStackTrace();
		}
		return Profiles;
	}

	
     
	public static void main(String[] args) {
		List l = ReadFile("C:\\Users\\Priyanka More\\Workspace\\RestaurantAssignment\\Files to Read\\WorkProfiles.txt");

		Iterator<String> itr = l.iterator();
		while (itr.hasNext())
			System.out.println(itr.next());

		writeFile("C:\\Users\\Priyanka More\\Workspace\\RestaurantAssignment\\Files to Read\\WorkProfiles.txt",
				"Junior Accountant");

		writeFile("C:\\Users\\Priyanka More\\Workspace\\RestaurantAssignment\\Files to Read\\WorkProfiles.txt",
				"Tally Operator");

	}

}
