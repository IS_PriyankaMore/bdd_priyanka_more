package main.java.com.departments;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


//Java Concepts Used: Map ,Hash Map, Set ,Iterator 
public class Bar {

	public static void main(String[] args) {
		addNewEmployeeinThisDepartment();
	}

	public static void addNewEmployeeinThisDepartment() {
		System.out.println("List of Employees Working in this Department :");
		Map map = new HashMap();
		map.put(1, "Cashier");
		map.put(5, "Waiter");
		map.put(2, "Servicing Man");
		map.put(6, "Assistant Manager");
		map.put(3, "Manager");
		Set set = map.entrySet();
		Iterator itr = set.iterator();
		while (itr.hasNext()) {
			Map.Entry entry = (Map.Entry) itr.next();
			System.out.println(entry.getKey() + " " + entry.getValue());
	
		}
		map.remove(3);  
		System.out.println("After removing one employee Manager");  
		
		Set set1 = map.entrySet();
		Iterator itr1 = set1.iterator();
		while (itr1.hasNext()) {
			Map.Entry entry1 = (Map.Entry) itr1.next();
			System.out.println(entry1.getKey() + " " + entry1.getValue());
		}
		}
		}

