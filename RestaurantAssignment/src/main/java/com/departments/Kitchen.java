package main.java.com.departments;

import java.util.Iterator;
import java.util.LinkedList;

//Java Concepts Used: Linked List,Iterator
public class Kitchen {

	public static void main(String args[]) {
		addNewEmployeeinThisDepartment();
	}

//Method to add types of employees in this department
	public static void addNewEmployeeinThisDepartment() {

		System.out.println("List of Employees Working in this Department :");
		LinkedList<String> al = new LinkedList<String>();
		al.add("Chefs");
		al.add("Cleaner");
		al.add("Helper");
		al.add("Butler");
		al.add("Assistant Manager");
		al.add("Cook");
		al.add("Order Provider");
		al.add("Waiter");
		Iterator<String> itr = al.iterator();
		while (itr.hasNext()) {
			System.out.println(itr.next());
		}
	}
}
