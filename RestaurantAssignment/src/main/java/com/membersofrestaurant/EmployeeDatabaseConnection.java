package main.java.com.membersofrestaurant;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//Java Concepts Used : JDBC Connection with MySql Data base
public class EmployeeDatabaseConnection {
	static Statement stmt = null;
	static ResultSet rs = null;
	static Connection con = null;

	public static void createConnection() throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeesofrestuarant", "root",
				"tiger");
		stmt = con.createStatement();
	}

	public static void viewAllEmplyoees() throws SQLException {

		try {
			createConnection();
			String query = "select * from emp;";

			rs = stmt.executeQuery(query);
			while (rs.next())

			{
				int ID = rs.getInt("id");
				int Salary = rs.getInt("salary");
				int Age = rs.getInt("age");
				String Name = rs.getString("name");
				String Department = rs.getString("department");

				System.out.println("Name: " + Name + "\t" + "ID:" + ID + "\t" + "Age : " + Age + "\t" + "Salary : "
						+ Salary + "\t" + "Department : " + Department);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

		finally {
			if (stmt != null) {
				stmt.close();
			}
		}

	}

	public static void insertIntoEmployeeTable() {
		try {
			createConnection();
			String sql = "INSERT INTO emp " + "VALUES (1, 'Jyo', 31,55000, 'Kitchen')";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO emp " + "VALUES (5, 'Zara', 30,50000, 'Dining')";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO emp " + "VALUES (6, 'Zoya', 32,30000, 'Reception')";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO emp " + "VALUES (7, 'Rohan', 20,20000, 'Accounting')";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO emp " + "VALUES (8, 'Suraj', 35,40000, 'Management')";
			stmt.executeUpdate(sql);
			sql = "INSERT INTO emp " + "VALUES (11, 'Suraja', 35,40000, 'Management')";
			stmt.executeUpdate(sql);
			System.out.println("Inserted records into the table...");

		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null)
					con.close();
			} catch (SQLException se) {
			}
			try {
				if (con != null)
					con.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

	}

	public static void deleteFromEmployeeTable() throws Exception {

		createConnection();
		String sql = "DELETE FROM emp " + "WHERE id = 11";
		stmt.executeUpdate(sql);
		System.out.println("Deleted record from the table...");
		viewAllEmplyoees();

	}

	public static void updateEmplyoees() throws SQLException {
		try {
			createConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		String sql = "UPDATE emp " + "SET age = 35 WHERE id in (2)";
		stmt.executeUpdate(sql);
		System.out.println("Updated record in  the table...");
		viewAllEmplyoees();
	}

	public static void main(String args[]) throws Exception {
		
		//Call methods as per requirement
		viewAllEmplyoees();
		insertIntoEmployeeTable();
		deleteFromEmployeeTable();
		updateEmplyoees();
		viewAllEmplyoees();

	}
}
