package main.java.com.membersofrestaurant;

import java.util.*;

//Java Concepts Used: Inheritance ,Constructor,Array List,Method Overloading and Overriding
public class ManagerDetails extends EmployeesDetails {
	public String name;
	private String username;
	private String password;

	private static Scanner input;
	protected static ArrayList<ManagerDetails> managers = new ArrayList<ManagerDetails>(); //

	public ManagerDetails() {
		this("", "", "");
	}

	public ManagerDetails(String username, String password) {
		this("", username, password);
	}

	public ManagerDetails(String name, String username, String password) {
		this.name = name;
		this.username = username;
		this.password = password;
	}

	public void createNewManager() {

		receiveCredentials();

		boolean isManagerExist = false;
		for (ManagerDetails manager : managers) {
			if (manager.name.equals(name)) {
				isManagerExist = true;
				System.out.println("\nManager Account Already Exists!\n");
				break;
			}
		}
		if (!isManagerExist) {
			managers.add(this);
			System.out.println("New Manager Account Successfully Created.");
			displayManager();
		}
	}

	public void receiveCredentials() {
		input = new Scanner(System.in);
		System.out.println("\nCreate New Manager Account here \n--------------------------");
		System.out.print("Enter manager name here: ");
		name = input.nextLine();
		System.out.print("Enter new username here: ");
		username = input.nextLine();
		System.out.print("Enter new password here: ");
		String passwordFirstTyped = input.nextLine();
		System.out.print("Retype new password here: ");
		String passwordRetyped = input.nextLine();
		while (!passwordFirstTyped.equals(passwordRetyped)) {
			System.out.println("Retyped password does not match! Try again.");
			System.out.print("Enter new password: ");
			passwordFirstTyped = input.nextLine();
			System.out.print("Retype new password: ");
			passwordRetyped = input.nextLine();
		}
		password = passwordFirstTyped;
	}

	public boolean isValidManager() {
		boolean isValid = false;
		for (ManagerDetails manager : managers) {
			if (manager.username.equals(username) && manager.password.equals(password))
				isValid = true;
		}
		return isValid;
	}

	public void displayManager() {
		System.out.println("\nAvailable Managers List\n-------------");
		int count = 1;
		for (ManagerDetails manager : managers) {
			System.out.print(count + ". Manager Name: ");
			System.out.println(manager.name);
			System.out.print("   Manager Username: ");
			System.out.println(manager.username);
			count++;
		}
	}

	public void deleteManager() {
		boolean isExist = false;
		ManagerDetails manager = null;
		if (managers.size() > 1) {
			System.out.print("\nEnter manager name to be deleted: ");
			String name = input.nextLine();
			for (ManagerDetails managerL : managers) {
				if (managerL.name.equals(name)) {
					isExist = true;
					manager = managerL;
					break;
				}
			}
		} else {
			System.out.println("Manager Account Deletion aborted! Only one manager account exist.");
			return;
		}
		if (isExist) {
			managers.remove(manager);
			System.out.println("Specified Manager Account Successfully Deleted.");
		} else
			System.out.println("Specified Manager Account Does Not Exist!");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public void addEmployee() {
		super.addEmployee();
	}

	@Override
	public void displayEmployeeDetails() {
		super.displayEmployeeDetails();
	}

	@Override
	public void removeEmployee() {
		super.removeEmployee();
	}
}
