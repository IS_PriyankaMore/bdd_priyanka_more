package main.java.com.membersofrestaurant.owner;

import java.util.Scanner;

public class OwnerDetails extends main.java.com.membersofrestaurant.ManagerDetails {

	private static String name;
	private static String address;
	private static String dob; // please enter in format DD/MM/YYYY
	private static String gender;

	public static void addOwner() {
		System.out.println("Add Owner Here>>>");
		Scanner sc = new Scanner(System.in); // Create a Scanner object
		System.out.println("Enter name");
		name = sc.nextLine();
		System.out.println("Enter address");
		address = sc.nextLine();
		System.out.println("Enter gender");
		gender = sc.nextLine();
		System.out.println("Enter DOB");
		dob = sc.nextLine();
	}

	public static void displayOwnerDetails() {
		System.out.println("Owner Added Successfully>>>>>>");
		System.out.println("This are the details you entered : ");
		System.out.println("Employee name: " + name);
		System.out.println("Address: " + address);
		System.out.println("D.O.B: " + dob);
		System.out.println("Gender: " + gender);

	}
	public static void main(String[] args) {
		addOwner();
		displayOwnerDetails();
	}
}
