package main.java.com.membersofrestaurant.owner;

import main.java.com.membersofrestaurant.EmployeesDetails;
import main.java.com.membersofrestaurant.ManagerDetails;

public class OwnerAuthorities extends main.java.com.membersofrestaurant.ManagerDetails {

	// Owner can add ,remove manager and employees here
	@Override
	public void createNewManager() {
		super.createNewManager();
	}

	@Override
	public void displayManager() {
		super.displayManager();
	}

	@Override
	public void deleteManager() {
		super.deleteManager();
	}

	@Override
	public void addEmployee() {
		super.addEmployee();
	}

	@Override
	public void displayEmployeeDetails() {
		super.displayEmployeeDetails();
	}

	@Override
	public void removeEmployee() {
		super.removeEmployee();
	}

	public static void main(String[] args) {

		// Employees Related Authorities
		EmployeesDetails emp[] = new EmployeesDetails[5];

		for (int i = 0; i < 5; i++) {

			emp[i] = new EmployeesDetails();
			emp[i].addEmployee();
		}

		System.out.println("**** Data Entered as below ****");

		for (int i = 0; i < 5; i++) {

			emp[i].displayEmployeeDetails();
		}

		// Manager related Authorities
		ManagerDetails manager[] = new ManagerDetails[2];

		for (int i = 0; i < 2; i++) {

			manager[i] = new ManagerDetails();
			manager[i].createNewManager();
		}

		System.out.println("**** Data Entered as below ****");

		for (int i = 0; i < 2; i++) {

			manager[i].displayManager();
		}
	}
}
