package main.java.com.customers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//Java Concepts Used : Thread , Buffer reader ,Exception,Interface and Inheritance
class CustomerOrder {

	public static void main(String args[]) throws Exception {

		CustomerOrder order = new CustomerOrder();
		takeOrder ord = new takeOrder(order);
		dispOrder ord1 = new dispOrder(order);

	}

	boolean valueset = false;
	String str[] = new String[3];

	synchronized void d_takeOrder(Thread t) {
		if (valueset) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println(e);
			}
		}
		System.out.println("\n" + t);
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			for (int i = 0; i < 3; i++) {
				System.out.print("\n Take an Order " + (i + 1) + " :: ");
				str[i] = br.readLine();
			}
		} catch (IOException e) {
			System.out.println(e);
		}

		valueset = true;
		notify();
	}

	synchronized void d_dispOrder(Thread t) {
		if (!valueset) {
			try {
				wait();
			} catch (InterruptedException e) {
				System.out.println(e);
			}
		}
		System.out.println("\n" + t);
		for (int i = 0; i < 3; i++) {
			System.out.print("\n Place an Order " + (i + 1) + " :: " + str[i]);
		}
		valueset = false;
		notify();
	}

}

class takeOrder implements Runnable {
	CustomerOrder custorder;
	Thread t;

	takeOrder(CustomerOrder custorder) {
		this.custorder = custorder;
		t = new Thread(this, "Manager take an order");
		t.start();
	}

	public void run() {
		for (int i = 0; i < 2; i++) {
			custorder.d_takeOrder(t);
		}
	}
}

class dispOrder implements Runnable {
	CustomerOrder custorder;
	static Thread t;

	dispOrder(CustomerOrder custorder) {
		this.custorder = custorder;
		t = new Thread(this, "Manager place an order");
		t.start();
	}

	public void run() {
		for (int i = 0; i < 2; i++) {
			custorder.d_dispOrder(t);
		}
	}

}
